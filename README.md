# Force Time Series DLL

Force DLL that linearly interpolates a time series, and applies it as a force and moment at a given node.

There are two subroutines available, namely: `ForceTimeSeries` and `DampVelTimeSeries`. `ForceTimeSeries` is meant to apply an external force or moment that only depends on time
```math
\bm{F}_{\textrm{applied}} = \bm{F}_{\textrm{external}}(t),
\\
\bm{M}_{\textrm{applied}} = \bm{M}_{\textrm{external}}(t).
```

Instead, `DampVelTimeSeries` will apply a force or moment that is proportional, and opposite, to the node linear and angular velocities.
```math
\bm{F}_{{\textrm{applied}}_i} = - \bm{F}_{{\textrm{external}}_i}(t) \bm{v}_i(t) \quad \forall i \in [1, 3],
\\
\bm{M}_{{\textrm{applied}}_i} = - \bm{M}_{{\textrm{external}}_i}(t) \bm{\omega}_i(t) \quad \forall i \in [1, 3].
```

The DLL loads a file in ASCII format, with on the first line the number of time instants, and then each line contains  $`t`$, $`F_x`$, $`F_y`$, $`F_z`$, $`M_x`$, $`M_y`$, $`M_z`$. Forces and moments must be given in global coordinates. Since the body rotation matrix is not used, this DLL cannot be used for a follower force.

For example, for a pull and release simulation the input file might look like:
```
6
  0.0    0.0    0.0      0.0    0.0    0.0    0.0
100.0    0.0    0.0      0.0    0.0    0.0    0.0
105.0    0.0    1.0E5    0.0    0.0    0.0    0.0
135.0    0.0    1.0E5    0.0    0.0    0.0    0.0
136.0    0.0    0.0      0.0    0.0    0.0    0.0
999.0    0.0    0.0      0.0    0.0    0.0    0.0
```

The HAWC2 code to use `ForceTimeSeries` is:
```
begin force;
    begin dll;
        name ext_force;
        dll ./dll/ForceTimeSeries.dll;
        init forcetimeseries_init ./dll/pull_and_release.dat;
        update forcetimeseries_update;
        output forcetimeseries_output;
        output_label forcetimeseries_output_label;
        mbdy tower;
        node 11;
    end dll;
end force;
```
The `init` command must be present, but the file name can be omitted. In this case the input file defaults to 'force_time_series.txt'.
The `output` command is needed to send the dll output to the HAWC2 results file. Each component can be labeled by adding the optional `output_label` command.

A similar code works for `DampVelTimeSeries`:
```
begin force;
    begin dll;
        name ext_force;
        dll ./dll/ForceTimeSeries.dll;
        init forcetimeseries_init ./dll/pull_and_release.dat;
        update dampveltimeseries_update;
        output forcetimeseries_output;
        output_label forcetimeseries_output_label;
        mbdy tower;
        node 11;
    end dll;
end force;
```

The output of this dll is available in the HAWC2 output by writing in the htc file:
```
begin output;
; ...
  force ext_force;
; ...
end output;
```
Where the argument of the `force` command is the `name` of the dll.

It is possible to specify an arbitrary number of force dll. To do that, one should duplicate the dll file and change its name.

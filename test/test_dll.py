# %% Import.

from ctypes import CDLL, ARRAY, POINTER, byref, c_char, c_double, create_string_buffer
import time as time_mod
import numpy as np
from scipy import interpolate
from scipy import signal
from numpy.ctypeslib import ndpointer
# import matplotlib.pyplot as plt

# plt.close('all')


# %% Load DLL and setup signatures.

dll = CDLL('./../ForceTimeSeries/x64/Debug/ForceTimeSeries.dll')


dll.forcetimeseries_init.argtypes = [ARRAY(c_char, 256)]
dll.forcetimeseries_init.restype = None

dll.forcetimeseries_update.argtypes = [
    POINTER(c_double),  # time
    ndpointer(shape=(3), dtype=c_double, flags="FORTRAN"),  # x
    ndpointer(shape=(3), dtype=c_double, flags="FORTRAN"),  # xdot
    ndpointer(shape=(3), dtype=c_double, flags="FORTRAN"),  # xdot2
    ndpointer(shape=(3, 3), dtype=c_double, flags="FORTRAN"),  # amat
    ndpointer(shape=(3), dtype=c_double, flags="FORTRAN"),  # omega
    ndpointer(shape=(3), dtype=c_double, flags="FORTRAN"),  # omegadot
    ndpointer(shape=(3), dtype=c_double, flags="FORTRAN"),  # f
    ndpointer(shape=(3), dtype=c_double, flags="FORTRAN"),  # m
    ]
dll.forcetimeseries_update.restype = None


# %% Make test data.

# time = np.array([0.0, 1.0, 1.5, 2.0, 3.0, 4.0, 10.0])
time = np.arange(0.0, 1000.0, 0.1)
table = np.zeros((time.size, 7))
table[:, 0] = time
table[:, 1] = 2.5 * time - 10.0
table[:, 2] = np.sin(time)
table[:, 3] = time ** 2
table[:, 4] = - np.sqrt(time)
# table[:, 5] = signal.square(2.0 * time)

input_file_name = './table_input.txt'
np.savetxt(input_file_name, table,
           fmt='%+.6e',
           header=f'{time.size}', comments='')


# %% Make reference results.

time_eval = np.arange(time[0], time[-1], 0.01)
interpolator = interpolate.interp1d(
    table[:, 0], table[:, 1:],
    axis=0,
    copy=False,
    assume_sorted=True)
y_eval_reference = interpolator(time_eval)


# %% Test.

# Load test data.
buffer = create_string_buffer(
    bytes(input_file_name.ljust(256), 'ascii'), 256)
dll.forcetimeseries_init(buffer)

# Input data.
x = np.zeros((3), order='F')
xdot = np.zeros((3), order='F')
xdot2 = np.zeros((3), order='F')
amat = np.zeros((3, 3), order='F')
omega = np.zeros((3), order='F')
omegadot = np.zeros((3), order='F')
f = np.zeros((3), order='F')
m = np.zeros((3), order='F')

# Store the results. First 3 columns for f and last 3 for m.
y_eval_dll = np.zeros((time_eval.size, 6))

# Do the interpolation.
start_time = time_mod.time()
for k in range(time_eval.size):
    time_k = c_double(time_eval[k])
    dll.forcetimeseries_update(byref(time_k), x, xdot, xdot2, amat, omega, omegadot, f, m)
    y_eval_dll[k, :3] = f
    y_eval_dll[k, 3:] = m
print(f'Elapsed time: {time_mod.time() - start_time:.4f} s')


# Plot.
# fig, ax = plt.subplots(dpi=300)
# ax.set_xlabel('Time [s]')
# ax.set_ylabel('Signal')
# for i in range(6):
#     ax.scatter(time, table[:, i+1], color=f'C{i}', label=f'Input {i}')
#     ax.plot(time_eval, y_eval_dll[:, i], color=f'C{i}', label=f'Iterpolated DLL {i}')
#     # ax.plot(time_eval, y_eval_reference[:, i], color=f'C{i}', label=f'Iterpolated Scipy {i}')
# 
# fig.savefig('./plot.png')

# Check against scipy implementation.
np.testing.assert_allclose(y_eval_dll, y_eval_reference, rtol=1e-06, atol=1e-6)

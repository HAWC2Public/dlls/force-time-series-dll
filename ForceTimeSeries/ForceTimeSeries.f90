module ForceTimeSeriesMod
!
! Module for applying a force/moment by interpolating the time series read in a file.
!
! How to compile on Linux:
!     mkdir -p ./Linux/Release/
!     ifort -fpp -shared -fPIC -O3 -Wl,-rpath -Wl,'$ORIGIN'/lib -Wl,-rpath -Wl,'$ORIGIN' ./ForceTimeSeries.f90 -o ./Linux/Release/ForceTimeSeries.so
!

use, intrinsic :: iso_c_binding, only: c_int, c_double, c_char

implicit none

!****************************************************************************************
! Module variables.
!****************************************************************************************

! Path to the file that contains the time series.
! The file is in ASCII format.
! The first line contains the number of rows.
! The following lines contain:
!    t, fx, fy, fz, mx, my, mz
character(len=*, kind=c_char), parameter :: default_input_file = './force_time_series.txt'
character(len=:, kind=c_char), allocatable :: input_file

! Input time series.
! - grid_x contains the time instants.
! - grid_y contains the forces and moments. Each column contains: fx, fy, fz, mx, my, mz
real(kind=c_double), dimension(:),    allocatable :: grid_x
real(kind=c_double), dimension(:, :), allocatable :: grid_y

! Interpolated values
real(kind=c_double), dimension(6) :: valinterp

contains


!****************************************************************************************
! Function to interpolate the forces/moments as defined in default_input_file to the given time.
!****************************************************************************************
function lerp(time) result(y)

    real(kind=c_double), intent(in) :: time
    
    ! Interpolated values at the current time.
    real(kind=c_double), dimension(6) :: y
    
    ! Time index in input_timeseries before the given time.
    ! Saved, so that we always start from the last value that has been found.
    integer(kind=c_int) :: k = 2

    ! Relative distance of time from the grid point before it.
    real(kind=c_double) :: w
    
    ! Look for the time index.
    do k = k, size(grid_x)
        if (grid_x(k) >= time) exit
    end do
    k = k - 1

    ! Compute weight.
    w = (time - grid_x(k)) / (grid_x(k+1) - grid_x(k))

    ! Do the linear interpolation.
    y =  w * grid_y(:, k+1) + (1.0_c_double - w) * grid_y(:, k)

    ! Update time index for next call.
    k = k + 1
    
end function lerp



!****************************************************************************************
! Initialization subroutine that is called before the simulation starts.
!****************************************************************************************
subroutine ForceTimeSeries_init(init_string_arr) bind(C, name='forcetimeseries_init')
!DEC$ IF .NOT. DEFINED(__LINUX__)
    !DEC$ ATTRIBUTES DLLEXPORT :: forcetimeseries_init
!DEC$ ENDIF

implicit none

!****************************************************************************************
! Input variables.
!****************************************************************************************

! Input file name. If empty, defaults to default_input_file.
! Defined as an array of length 1 characters because of bind.
character(len=1), dimension(256), intent(in) :: init_string_arr

!****************************************************************************************
! Local variables.
!****************************************************************************************

! Same as init_string_arr, but as string, instead of an array of characters.
! See https://community.intel.com/t5/Intel-Fortran-Compiler/error-6360-A-scalar-valued-argument-is-required-in-this-context/td-p/805212
character(len=256, kind=c_char) :: init_string

! Unit to read the input file.
integer(kind=c_int) :: fid

! 0 if opening the input file succeeded.
integer(kind=c_int) :: open_err

! Number of time instants.
integer(kind=c_int) :: ntimes

! Input time series as read from file.
real(kind=c_double), dimension(:, :), allocatable :: table

!****************************************************************************************
! Read the input time series from file.
!****************************************************************************************

! Test for empty file name.
init_string = transfer(init_string_arr, init_string)
if (len_trim(init_string) > 0) then
    input_file = trim(init_string)
else
    input_file = default_input_file
end if

! Read the time series.
open(newunit=fid, file=input_file, form='FORMATTED', status='OLD', action='READ', iostat=open_err)
if (open_err == 0) then
    read(unit=fid, fmt='(I)') ntimes
    allocate(table(7, ntimes),   &
             grid_x(ntimes),     &
             grid_y(6, ntimes))
    read(unit=fid, fmt=*) table
    grid_x = table(1,:)
    grid_y = table(2:7,:)
    close(unit=fid)
    write(*, '(A,I0,A,A)') 'Read ', ntimes, ' lines from ',  input_file
else
    write(*, '(A,A,A,I0)') 'Error opening ', input_file, '. Error code = ', open_err  ! TODO: use logfile_unit
end if

end subroutine ForceTimeSeries_init



!****************************************************************************************
! Main subroutine, called during the simulation.
! Simply interpolate the forces/moments from default_input_file to the current time
! and return the interpolated values back to HAWC2
!****************************************************************************************
subroutine ForceTimeSeries_update(time, x, xdot, xdot2, amat, omega, omegadot, f, m)
!DEC$ ATTRIBUTES ALIAS: "forcetimeseries_update" :: forcetimeseries_update
!DEC$ IF .NOT. DEFINED(__LINUX__)
    !DEC$ ATTRIBUTES DLLEXPORT, STDCALL, REFERENCE :: forcetimeseries_update
!DEC$ ENDIF

implicit none

!****************************************************************************************
! Input variables.
!****************************************************************************************

! Time
real(kind=c_double), intent(in) :: time
! Position of reference node in global reference frame.
real(kind=c_double), dimension(3), intent(in) :: x
! Velocity of reference nodein global reference frame.
real(kind=c_double), dimension(3), intent(in) :: xdot
! Acceleration of reference node in global reference frame.
real(kind=c_double), dimension(3), intent(in) :: xdot2
! Rotation tensor from body to global reference frame.
real(kind=c_double), dimension(3, 3), intent(in) :: amat
! Angular velocity of reference node in global reference frame.
real(kind=c_double), dimension(3), intent(in) :: omega
! Angular acceleration of reference node in global reference frame.
real(kind=c_double), dimension(3), intent(in) :: omegadot

!****************************************************************************************
! Output variables.
!****************************************************************************************

! External force applied to reference node in global reference frame.
real(kind=c_double), dimension(3), intent(out) :: f
! External moment applied to reference node in global reference frame.
real(kind=c_double), dimension(3), intent(out) :: m

!****************************************************************************************
! Interpolate the time series.
!****************************************************************************************
valinterp = lerp(time)

!****************************************************************************************
! Rotate
!****************************************************************************************


!****************************************************************************************
! Return the interpolated force and moment.
!****************************************************************************************
f = valinterp(1:3)
m = valinterp(4:6)

end subroutine ForceTimeSeries_update



!****************************************************************************************
! Main subroutine, called during the simulation.
! Simply interpolate the forces/moments from default_input_file to the current time,
! multiply with the velocities (translation and rotation) to obtain a damping force/moment,
! add a minus sign (so positive input in default_input_file results in positive 
! a positive damping force, and return values to HAWC2
!****************************************************************************************
subroutine DampVelTimeSeries_update(time, x, xdot, xdot2, amat, omega, omegadot, f, m)
!DEC$ ATTRIBUTES ALIAS: "dampveltimeseries_update" :: dampveltimeseries_update
!DEC$ IF .NOT. DEFINED(__LINUX__)
    !DEC$ ATTRIBUTES DLLEXPORT, STDCALL, REFERENCE :: dampveltimeseries_update
!DEC$ ENDIF

implicit none

!****************************************************************************************
! Input variables.
!****************************************************************************************

! Time
real(kind=c_double), intent(in) :: time
! Position of reference node in global reference frame.
real(kind=c_double), dimension(3), intent(in) :: x
! Velocity of reference nodein global reference frame.
real(kind=c_double), dimension(3), intent(in) :: xdot
! Acceleration of reference node in global reference frame.
real(kind=c_double), dimension(3), intent(in) :: xdot2
! Rotation tensor from body to global reference frame.
real(kind=c_double), dimension(3, 3), intent(in) :: amat
! Angular velocity of reference node in global reference frame.
real(kind=c_double), dimension(3), intent(in) :: omega
! Angular acceleration of reference node in global reference frame.
real(kind=c_double), dimension(3), intent(in) :: omegadot

!****************************************************************************************
! Output variables.
!****************************************************************************************

! External force applied to reference node in global reference frame.
real(kind=c_double), dimension(3), intent(out) :: f
! External moment applied to reference node in global reference frame.
real(kind=c_double), dimension(3), intent(out) :: m

!****************************************************************************************
! Interpolate the time series.
!****************************************************************************************

valinterp = lerp(time)


!****************************************************************************************
! Turn into damping force
!****************************************************************************************
valinterp(1:3) = -valinterp(1:3)*xdot(1:3)
valinterp(4:6) = -valinterp(4:6)*omega(1:3)


!****************************************************************************************
! Return values to HAWC2: interpolated damping coefficients multiplied with HAWC2.
!****************************************************************************************

f(1:3) = valinterp(1:3)
m(1:3) = valinterp(4:6)

end subroutine DampVelTimeSeries_update



!****************************************************************************************
! Output of this dll that will go into the HAWC2 results file.
!****************************************************************************************
subroutine forcetimeseries_output(nout, output_array)
!DEC$ ATTRIBUTES ALIAS: "forcetimeseries_output" :: forcetimeseries_output
!DEC$ IF .NOT. DEFINED(__LINUX__)
    !DEC$ ATTRIBUTES DLLEXPORT, STDCALL, REFERENCE :: forcetimeseries_output
!DEC$ ENDIF

! Dimension of the output array. Resquested by HAWC2 during the initialization phase.
integer(kind=c_int), intent(inout) :: nout
! Output array, with nout elements.
real(kind=c_double), dimension(:), intent(out)  :: output_array

! Initialise on first call.
if (nout == 0) then
  nout = 6
else
  output_array = valinterp
endif

end subroutine forcetimeseries_output


subroutine forcetimeseries_output_label(nr, string)
!DEC$ ATTRIBUTES ALIAS: "forcetimeseries_output_label" :: forcetimeseries_output_label
!DEC$ IF .NOT. DEFINED(__LINUX__)
    !DEC$ ATTRIBUTES DLLEXPORT :: forcetimeseries_output_label
!DEC$ ENDIF

! Component index.
integer, intent(in) :: nr
! Component name.
character(len=1), dimension(256), intent(out) :: string

select case (nr)
    case (1)
        string(1:2) = ['F', 'x']
    case (2)
        string(1:2) = ['F', 'y']
    case (3)
        string(1:2) = ['F', 'z']
    case (4)
        string(1:2) = ['M', 'x']
    case (5)
        string(1:2) = ['M', 'y']
    case (6)
        string(1:2) = ['M', 'z']
    case default
        string(1) = '?'
end select


end subroutine forcetimeseries_output_label

end module

